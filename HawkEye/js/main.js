var count = 3;
var r;
var g;
var b;
var td;
var tdArray = [];
var trArray = [];
var time = performance.now();
var points =0;
//DataBase
var db;
var version = 1;
var dbName = "taskdb";
var dbDisplayName = "tizen_task_db";
var dbSize = 2 * 1024 * 1024;
var answers = ["Ghost 3422     Level: 7","Ghost 3422     Level: 7","Ghost 3422     Level: 7"];
fillArrays();
start();
( function () {

    window.addEventListener( 'tizenhwkey', function( ev ) {
        if( ev.keyName === "back" ) {
            var activePopup = document.querySelector( '.ui-popup-active' ),
                page = document.getElementsByClassName( 'ui-page-active' )[0],
                pageid = page ? page.id : "";
            
            if( pageid === "one" && !activePopup ) {
                try {
                    tizen.application.getCurrentApplication().exit();
                } catch (ignore) {
                }
            } else {
                window.history.back();
            }
        }
    } );
} () );

function randomNumber(max, min) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
function fillArray(num, max, min){
	var arr = new Array();
	for (var i=0; i < num; i++){
		arr[i] = randomNumber(max, min);
	}
}
function fillArrays(){
	for(var i = 0; i < 50; i++){
		trArray[i] = document.createElement('tr');
		trArray[i].setAttribute('id','tr'+i);
	}
	for(var i = 0; i < 2500; i++){
		tdArray[i] = document.createElement('td');
		tdArray[i].setAttribute('id','td'+i);
		
	}
}
function start(){
	r = randomNumber(255, 0);
	g = randomNumber(255, 0);
	b = randomNumber(255, 0);
	td = randomNumber(3,0);
	more(2);
}
function more(num){
	var elementCount = 0;
	$('#table').remove();
	//table.parentNode.removeChild(table);
	var table = document.createElement('table')
	table.setAttribute('id','table');
	div0.appendChild(table);
	for(var i = 0; i < num; i++){
		table.appendChild(trArray[i]);
		for (var n = 0; n < num;n++){
			trArray[i].appendChild(tdArray[elementCount]);
			changeColor('td'+elementCount,r, g, b);
			changeColor('td'+td,r+10, g-10, b+10);
			
			elementCount++;
		}
		}
	$('#table').bind('click', function(){
		lol();
		add();
	});
	$('#td'+td).bind('click', function() {
		r = randomNumber(255, 0);
    	g = randomNumber(255, 0);
    	b = randomNumber(255, 0);
    	td = randomNumber(count*count,1);
    	more(count);
    	count++;
    	time = performance.now() - time;
    	if (count==2){
    		points = 100;
    		document.getElementById("count").innerHTML = points;
    	}
    	else{
    		points= points + Math.floor(((200*count)/time)*500);
    		document.getElementById("count").innerHTML = points;
    	}
    	time = performance.now();
    	document.getElementById("level").innerHTML = count-2;
	});
}

function changeColor(id, red, green, blue){
		 $('#'+id).css('backgroundColor', 'rgb('+red+','+green+','+blue+')');		
}
function lol(){
	location.href = "#three";
	fillArrays();
	start();
	document.getElementById("count").innerHTML = 0;
	document.getElementById("level").innerHTML = 1;
	count = 2;
	points=0;
}
function add(){
	 if(answers.length!=0){
	    	for(var i = 0; i <answers.length;i++){
	    		$("#listView1")
	    		.append('<li>'+answers[i]+'</li>')
	    		.listview('refresh');
	    	}
	    }
	    $('#btnAdd').bind(
	    		'click'
	    		,function(){
	    	$("#listView1")
	    		.append('<li>'+$('#text1').val()+'</li>')
	    		.listview('refresh');
	    })
}